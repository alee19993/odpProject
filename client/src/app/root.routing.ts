import {NgModule} from '@angular/core';
import {CommonModule,} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthComponent} from "./auth/auth.component";
import {AppComponent} from "./main-app/app.component";
import {PresentationComponent} from "./presentation/presentation.component";

const routes: Routes = [
    {path: 'authentication', component: AuthComponent},
    {path: 'home', component: PresentationComponent},
    {path: 'app', component: AppComponent, children:[ {path:'',loadChildren: './main-app/app.module#AppModule'}]},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
})
export class RootRoutingModule {
}
