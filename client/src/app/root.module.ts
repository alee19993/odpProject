import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {AppComponent} from "./main-app/app.component";
import {AppRoutingModule} from "./main-app/app.routing";
import {AuthComponent} from "./auth/auth.component";
import {ComponentsModule} from "./main-app/components/components.module";
import {RootRoutingModule} from "./root.routing";
import {AppModule} from "./main-app/app.module";
import {RootComponent} from "./root.component";
import {PresentationComponent} from "./presentation/presentation.component";


@NgModule({
    declarations: [
        AppComponent,
        AuthComponent,
        PresentationComponent,
        RootComponent

    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ComponentsModule,
        RouterModule,
        RootRoutingModule
    ],
    providers: [],
    bootstrap: [RootComponent]
})
export class RootModule {
}
