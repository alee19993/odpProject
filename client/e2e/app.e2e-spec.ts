import { MaterialDashboardAngularPage } from './app.po';

describe('material-dashboard-angular App', () => {
  let page: MaterialDashboardAngularPage;

  beforeEach(() => {
    page = new MaterialDashboardAngularPage();
  });

  it('should display message saying main-app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('main-app works!');
  });
});
