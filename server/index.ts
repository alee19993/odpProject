import app from './src1';
import config from './src1/config';
import logger from './src1/util/logger';

app.listen(config.port, () => {
    logger.log(`server started at port ${config.port}`);
});
