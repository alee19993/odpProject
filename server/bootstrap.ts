import 'reflect-metadata';
import {interfaces, InversifyExpressServer, TYPE} from 'inversify-express-utils';
import {Container} from 'inversify';
import {makeLoggerMiddleware} from 'inversify-logger-middleware';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import TYPES from './src/constant/types';
import {UserService} from './src/service/user.service';
import {UserController} from './src/controller/user.controller';
import config from './src/config';
import {UserDal} from './src/models/user.model';
import {LocalAuthStrategy} from './src/auth/strategies/local-strategy';
import {MongoConnection} from './src/wrappers/mongoconnection';
import {AuthController} from './src/controller/auth.controller';
import TAGS from './src/constant/tags';
import {passport} from './src/wrappers/passport';
import * as path from 'path';
import {static as stat} from 'express';
import {logger} from './src/utils/logger/logger';
var winston = require('winston'),
    expressWinston = require('express-winston');
const cookieParser = require('cookie-parser');
const session = require('express-session');
// load everything needed to the Container
let container = new Container();

if (process.env.NODE_ENV === 'development') {
    let logger = makeLoggerMiddleware();
    container.applyMiddleware(logger);
}
// controller
container.bind<AuthController>(TAGS.AuthController).to(AuthController).inSingletonScope();
container.bind<UserController>(TAGS.UserController).to(UserController).inSingletonScope();

// dal
container.bind<UserDal>(TYPES.UserModel).to(UserDal);
// services
container.bind<UserService>(TYPES.UserService).to(UserService).inSingletonScope();
container.bind<MongoConnection>(TYPES.MongoConnection).to(MongoConnection).inSingletonScope();

// utils
// container.bind(TwitterAuthStrategy).to(TwitterAuthStrategy).inSingletonScope();
// container.bind(GoogleAuthStrategy).to(GoogleAuthStrategy).inSingletonScope();
container.bind(LocalAuthStrategy).to(LocalAuthStrategy).inSingletonScope();
container.get(TYPES.MongoConnection);
container.get(LocalAuthStrategy);

// start the server
let server = new InversifyExpressServer(container);

let redirect = '../client-back-office/dist/index.html'

server.setConfig((app) => {
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.header('Access-Control-Allow-Credentials', 'true');
        next();
    });
    app.use(helmet());
    app.use(session({
        secret: 'keyboard cat',
        cookie: {secure: 'auto', httpOnly: false},
        resave: true,
        saveUninitialized: true,
    }));
    app.use(stat(path.join(__dirname, '../client/dist')));

    if(config.environment === 'development'){
        app.use(stat(path.join(__dirname, '../client-front-office/')));
        app.use(stat(path.join(__dirname, '../client-back-office/dist')));


    } else {
        app.use(stat(path.join(__dirname, '../../client-front-office/')));
        app.use(stat(path.join(__dirname, '../../client-back-office/dist')));

    }


    app.use(passport.initialize());   // passport initialize middleware
    app.use(passport.session());
    app.use(cookieParser());
    app.use(expressWinston.logger({
        transports: [
            new winston.transports.Console({
                json: false,
                colorize: true,
                timestamp: true
            })
        ],
        level: function (req: any, res: any) {
            var level = "";
            if (res.statusCode >= 100) { level = "info"; }
            if (res.statusCode >= 400) { level = "warn"; }
            if (res.statusCode >= 500) { level = "error"; }
            // Ops is worried about hacking attempts so make Unauthorized and Forbidden critical
            if (res.statusCode == 401 || res.statusCode == 403) { level = "critical"; }
            // No one should be using the old path, so always warn for those
            return level;
        },
        meta: true, // optional: control whether you want to log the meta data about the request (default to true)
        expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
        colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
        ignoreRoute: function (req: any , res: any) { return false; } // optional: allows to skip some log messages based on request and/or response
    }))

});


if(config.environment === 'development'){
    redirect = '../client-back-office/dist/index.html'


} else {
    redirect = '../../client-back-office/dist/index.html'
}
let app = server.build();
app.listen(config.port);
app.get('/*', function (req, res) {
    if (!res.headersSent) {
        res.sendFile(path.join(__dirname, '../client/dist/index.html'));
    }
});
logger.info('Server started on port: ' + config.port);
logger.info('Env: ' + config.environment);
exports = module.exports = app;