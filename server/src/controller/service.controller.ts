import {controller, httpPost, httpGet, httpPut, httpDelete} from "inversify-express-utils";
import {inject} from "inversify";
import TYPES from "../constant/types";
import {ServiceService} from "../service/service.service";
import {IService} from "../interfaces/service.interface";
import {Service} from "../models/service.model";
import {Request, Response} from 'express';


@controller('/service')
export class ServiceController{
    constructor(@inject(TYPES.ServiceService) private serviceService: ServiceService){}

    @httpPost('/')
    async create(request: Request, response: Response): Promise<any> {
        try {
            return await this.serviceService.create(request.body)
        } catch (e) {

        }
    }
    @httpGet('/')
    async getAll(request: Request, response: Response): Promise<any> {
        try {
            return await this.serviceService.getAll()

        } catch (e) {

        }
    }
    @httpGet('/:serviceId')
    async getServiceById(request: Request, response: Response): Promise<any> {
        try {
            return await this.serviceService.getServiceById(request.params.serviceId)

        } catch (e) {

        }
    }
    @httpPut('/')
    async update(request: Request, response: Response): Promise<any> {
        try {
            return await this.serviceService.update(request.body)

        } catch (e) {

        }
    }
    @httpDelete('/:serviceId')
    async delete(request: Request, response: Response): Promise<any> {
        try {
            return await this.serviceService.delete(request.params.serviceId)

        } catch (e) {

        }
    }


}