import {controller, httpGet, httpPost} from 'inversify-express-utils';
import {UserService} from '../service/user.service';
import {inject} from 'inversify';
import TYPES from '../constant/types';
import {passport} from '../wrappers/passport';
import {Request, Response} from 'express';
import {ErrorModel} from '../models/error.model';

@controller('/auth')
export class AuthController {
    constructor(@inject(TYPES.UserService) private userService: UserService) {
    }

    // local sign up sign in strategies
    @httpGet('me')
    async getCurrenntUser (request: Request, response: Response,){
        try {
            if (request.isAuthenticated() && request.user ) {
                response.json({
                    data: request.user
                });
            } else {
                throw new ErrorModel('noAuth');
            }

        } catch (e) {
            this.responseErrorHandler(response, e);
        }
    }
    @httpPost('/local/signup')
    async register(request: Request, response: Response, next: any): Promise<any> {
        try {
            let user = await this.__promisifiedPassportAuthentication('local-signup', request, response);
            response.json({
                data: user
            });

        } catch (e) {
            this.responseErrorHandler(response, e);
        }


    }

    /*  @httpGet('/local/logout')
      logout(): void {
      }*/

    @httpPost('/local/signin')
    async login(request: Request, response: Response, next: any): Promise<any> {
        try {
            if (!request.body.email) {
                throw  new ErrorModel('nomail');

            }
            if (!request.body.password) {
                throw new ErrorModel('nopsw');
            }
            let user = await this.__promisifiedPassportAuthentication('local-signin', request, response);
            if (user) {
                request.login(user, (error) => {
                    if (error) {
                        throw new ErrorModel();
                    }
                    response.json({
                        data: user
                    });
                });
            }

            /*response.cookie('currentUser', JSON.stringify(user),
                {maxAge: 900000, httpOnly: false});*/

        } catch (e) {
            this.responseErrorHandler(response, e); // ex: 'wrongPsw'
        }
    }

    private __promisifiedPassportAuthentication(strategyName: String, req: Request, res: Response): Promise<any> {
        return new Promise((resolve, reject) => {
            passport.authenticate(<string> strategyName, (err, user, info) => {
                if (!err) {
                    if (!info) {
                        resolve(user);
                    } else {
                        reject(new ErrorModel(info.message));
                    }
                } else {
                    reject(err);
                }
            })(req, res);  // <-- that guy right there
        });
    }

    @httpGet('/signout')
    public logout(request: any, response: Response, next: Function): void {
        request.logout();
        response.status(200).json({message: 'signedout'});
    }

    private responseErrorHandler(response: Response, error: ErrorModel): void {
        switch (error.name || error.message || error.code) {
            case 11000:
                // error 422 -> unprocessable entity
                response.status(422).json(error)
                break;
            case 'nodata':
                response.status(404).json(error)
                break;
            case 'missingcredentials':
                response.status(412).json(error)
                break;
            case 'wrongpsw':
                response.status(401).json(error)
                break;
            case 'nopsw':
                response.status(401).json(error)
                break;
            case 'nomail':
                response.status(401).json(error)
                break;
            case 'validationerror':
                response.status(412).json(error)
                break;
            default:
                response.status(500).send();
        }
    }

}