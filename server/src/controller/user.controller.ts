import {controller, httpDelete, httpGet, httpPost, httpPut} from 'inversify-express-utils';
import {inject} from 'inversify';
import {Request, Response} from 'express';
import {UserService} from '../service/user.service';
import TYPES from '../constant/types';
import {ErrorModel} from '../models/error.model';
import ROLES from '../constant/roles';

@controller('/user')
export class UserController {
    constructor(@inject(TYPES.UserService) private userService: UserService) {
    }

    @httpPost('/')
    async create(request: Request, response: Response): Promise<any> {
        try {
            if (request.isAuthenticated() && request.user && request.user.role === ROLES.Admin) {
                const result = await this.userService.create(request.body);
                response.json({
                    data: result
                });
            } else {
                throw new ErrorModel('noAuth');
            }
        } catch (e) {
            this.responseErrorHandler(response, e);
        }
    }

    @httpGet('/')
    async getAll(request: Request, response: Response): Promise<any> {
        try {
            if (request.isAuthenticated() && request.user && request.user.role === ROLES.Admin) {
                const result = await this.userService.getAll();
                response.json({
                    data: result
                });
            } else {
                throw new ErrorModel('noAuth');
            }
        } catch (e) {
            this.responseErrorHandler(response, e);
        }
    }

    @httpGet('/:userId')
    async getUserById(request: Request, response: Response): Promise<any> {
        try {
            if (request.isAuthenticated() && request.user && request.user.role === ROLES.Admin) {
                const result = await this.userService.getUserById(request.params.userId);
                response.json({
                    data: result
                });
            } else {
                throw new ErrorModel('noAuth');
            }


        } catch (e) {
            this.responseErrorHandler(response, e);
        }
    }

    @httpPut('/')
    async update(request: Request, response: Response): Promise<any> {
        try {
            if (request.isAuthenticated() && request.user && request.user.role === ROLES.Admin) {
                const result = await this.userService.update(request.body);
                response.json({
                    data: result
                });
            } else {
                throw new ErrorModel('noAuth');
            }
        } catch (e) {
            this.responseErrorHandler(response, e);
        }
    }

    @httpDelete('/:userId')
    async delete(request: Request, response: Response): Promise<any> {
        try {
            if (request.isAuthenticated() && request.user && request.user.role === ROLES.Admin) {
                const result = await this.userService.delete(request.params.userId);
                response.json({
                    data: result
                });
            } else {
                throw new ErrorModel('noAuth');
            }

        } catch (e) {
            this.responseErrorHandler(response, e);
        }
    }

    private responseErrorHandler(response: Response, error: ErrorModel): void {
        let e =  error.code || error.name || error.message;
        switch (e) {
            case 11000:
                // error 422 -> unprocessable entity
                error.message = 'duplicatekey'
                response.status(422).json(error)
                break;
            case 'validationerror':
                response.status(412).json(error);
                break;
            case 'noauth':
                response.status(401).json(error);
                break;
            default:
                response.status(500).send();
                break;
        }
    }
}
