export class ErrorModel {
    constructor(public name?: String,
                public code?: Number,
                public message?: String) {
        if(this.name) {
            this.name = this.name.trim().replace(/ +/g, '').toLowerCase();
        }
        if(this.message) {
            this.message = this.message.trim().replace(/ +/g, '').toLowerCase();
        }
    }
}