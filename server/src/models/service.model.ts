import {injectable} from 'inversify';
import {IService} from '../interfaces/service.interface';
import {arrayProp, prop, Typegoose} from 'typegoose';
import {ValidationError} from 'mongoose';
import {ErrorModel} from './error.model';
import {Binary} from "bson";


export class Service extends Typegoose {
    _id?: String;
    @prop({required: true, unique: true})
    name: String;
    @arrayProp({items: Binary})
    images: Binary[];
    @prop()
    mainImage: Binary;
    @prop()
    category: String;
    @prop()
    description: String;
}

const ServiceModel = new Service().getModelForClass(Service);

@injectable()
export class ServiceDal implements IService {
    async create(service: Service): Promise<Service> {
        try {
            const s = new ServiceModel(service);
            await s.save();
            return s;
        } catch (e) {
            if (e.name === 'ValidationError') {
                const keys = Object.keys(e.errors);
                throw new ErrorModel('validationerror', undefined, keys[0] + 'missing');
            } else {
                throw new ErrorModel(e.name, e.code);
            }
        }
    }

    async getAll(): Promise<Service[]> {
        try {
            return await ServiceModel.find();
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

    async getServiceById(serviceId: String): Promise<Service> {
        try {
            return <Service> await ServiceModel.findOne({_id: serviceId});
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

    async update(service: Service): Promise<Service> {
        try {
            let s = <Service> await ServiceModel.findOneAndUpdate({_id: service._id}, {$set: service});
            return s;

        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

    async delete(serviceId: String): Promise<Service> {
        try {
            let s = <Service> await ServiceModel.findOneAndRemove({_id: serviceId});
            return s;
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

    async getServicesByFilter(key: String, value: String | Number): Promise<Service[]> {
        try {
            const param = {
                [<string>key]: value
            };
            let s = await ServiceModel.find(param);
            return s;
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

}