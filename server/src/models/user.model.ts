import {injectable} from 'inversify';
import {IUser} from '../interfaces/user.interface';
import {prop, Typegoose} from 'typegoose';
import {ValidationError} from 'mongoose';
import {ErrorModel} from './error.model';


export class User extends Typegoose {
    _id?: String;
    @prop({required: true})
    fullname: String;
    @prop({required: true})
    providers: Array<String>;
    @prop({required: true, unique: true})
    email: String;
    @prop({required: true})
    password: String;
    @prop({required: true})
    role: Number;
}

const UserModel = new User().getModelForClass(User);


@injectable()
export class UserDal implements IUser {


    async create(nuser: User): Promise<User> {
        try {
            const user = new UserModel(nuser);
            await user.save();
            return user;
        } catch (e) {
            // if miss some required params
            /*
             email & password are mare managed by passport
             */
            if (e.name === 'ValidationError') {
                const keys = Object.keys(e.errors);
                throw new ErrorModel('validationerror', undefined, keys[0] + 'missing');
            } else {
                throw new ErrorModel(e.name, e.code);
            }
        }
    }

    async getAll(): Promise<User[]> {
        try {
            const users = await UserModel.find();
            return users;
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }

    }

    async getUserById(userId: String): Promise<User> {
        try {
            const user = <User> await UserModel.findOne({_id: userId});
            return user;
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }

    }

    async update(user: User): Promise<User> {
        try {
            let u = <User> await UserModel.findOneAndUpdate({_id: user._id}, {$set: user});
            return u;

        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

    async delete(userId: String): Promise<User> {
        try {
            let u = <User> await UserModel.findOneAndRemove({_id: userId});
            return u;
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

    async getUsersByFilter(key: String, value: Number | String): Promise<User[]> {
        try {
            const param = {
                [<string>key]: value
            };
            let u = await UserModel.find(param);
            return u;
        } catch (e) {
            throw new ErrorModel(e.name, e.code);
        }
    }

}

