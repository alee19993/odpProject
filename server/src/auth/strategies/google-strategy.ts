/*
import { passport } from '../../wrappers/Passport';
import * as GoogleStrategyLib from 'passport-google-oauth';
import { UserService } from '../service/UserService';
import { inject, injectable } from 'inversify';
import TYPES from '../constants/types';
import { logger } from '../../logger/logger';
import * as config from 'config';
import { UserRole, User } from '../models/UserModel';


@injectable()
export class GoogleAuthStrategy {
  constructor(
    @inject(TYPES.UserService) private userService: UserService
  ) {
    this.install();
    this.userService = userService;
  }
  public install() {
    const GoogleStrategy = GoogleStrategyLib.OAuth2Strategy;
    const uService = this.userService;

    passport.use(new GoogleStrategy({
      clientID: config.get('googleAuth.consumerKey'),
      clientSecret: config.get('googleAuth.consumerSecret'),
      callbackURL: config.get('googleAuth.callbackUrl')
    },
      function (accessToken, refreshToken, profile, done) {
        logger.debug('Persisting user after authentication.', {
          id: profile.id,
          user: profile.emails[0].value
        });

        uService
          .getUserWithProvider(profile.emails[0].value, 'google')
          .then(function (result) {
              const user = new User();
              // user.username = profile.displayName;
              user.username = profile.emails[0].value;
              user.email = profile.emails[0].value;
              user.role = UserRole.Maintainer;
              user.active = 1;

              if (result !== null) {
                user.role = result.role;
                user.id = result.id;
                user.active = result.active;
              }

              user.provider = profile.provider;
              user.identifier = profile.id;
              user.thumbnailurl = profile.photos.length > 0 ? profile.photos[0].value : null;
              user.bannerurl = profile._json.profile_banner_url;

              return uService.insertOrUpdate(user);
          })
          .then((user) => {
            logger.debug('Authentication has been validated, proceeding.', {
              username: user.username
            });

            done(null, user);
          })
          .catch(function (error) {
            logger.error('Error while persisting user.', {
              user: profile.username,
              failure: error.toString()
            });

            return done(null, false, {
              message: 'genericServerError'
            });
          });
      }
    ));
  }
}
*/
