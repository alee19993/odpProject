import {passport} from '../../wrappers/passport';
import {UserService} from '../../service/user.service';
import {inject, injectable} from 'inversify';
import TYPES from '../../constant/types';
import {User} from '../../models/user.model';
import {Request} from 'express';
import {ErrorModel} from '../../models/error.model';
const bcryptjs = require('bcryptjs');
const LocalStrategyLib = require('passport-local');


// import * as randtoken from 'rand-token';


@injectable()
export class LocalAuthStrategy {
    constructor(@inject(TYPES.UserService) private userService: UserService) {
        this.install();
        this.userService = userService;
    }

    public install(): void {

        const LocalStrategy = LocalStrategyLib.Strategy;
        const self = this;
        passport.use('local-signup', new LocalStrategy({
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true
            }, async (req: Request, fullname: String, password: String, done: Function) => {
                const user = new User();

                user.password = self.generateHash(password);
                user.fullname = req.body.fullname;
                user.email = req.body.email;
                user.role = 0;
                user.providers = ['local'];
                /*user.token = randtoken.generate(16);
                user.active = 0;*/
                try {
                    let u = await self.userService.create(user);
                    done(null, u);

                } catch (e) {
                    done(e , null);
                }
            }
        ))
        ;

        passport.use('local-signin', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        }, async (req: Request, mail: String, password: String, done: Function) => {
            try {
                let users = await self.userService.getUsersByFilter('email', mail);
                if (users.length === 1) {
                    const user = users[0];
                    if (self.validPassword(password, user.password)) {
                        done(null, user);
                    } else {
                        throw new ErrorModel('wrongPsw');
                    }
                } else {
                    throw new ErrorModel('noData');
                }
            } catch (e) {
                done(e, null);
            }
        }));


    }

    private generateHash(password: String): String {
        return bcryptjs.hashSync(password);
    }

    private validPassword(password: String, userPassword: String): Boolean {
        return bcryptjs.compareSync(password, userPassword);
    }
}


