/*
import { passport } from '../../wrappers/Passport';
import * as TwitterStrategyLib from 'passport-twitter';
import { UserService } from '../../service/UserService';
import { inject, injectable } from 'inversify';
import TYPES from '../../constants/types';
import { logger } from '../../logger/logger';
import * as config from 'config';
import { User, UserRole } from '../../models/UserModel';


@injectable()
export class TwitterAuthStrategy {
  constructor(
    @inject(TYPES.UserService) private userService: UserService
  ) {
    this.install();
    this.userService = userService;
  }

  public install(): void {

    const TwitterStrategy = TwitterStrategyLib.Strategy;
    const uService = this.userService;

    passport.use(new TwitterStrategy({
      consumerKey: config.get('twitterAuth.consumerKey'),
      consumerSecret: config.get('twitterAuth.consumerSecret'),
      callbackURL: config.get('twitterAuth.callbackUrl')
    },

      function (token, secret, profile, done) {
        logger.debug('Persisting user after authentication.', {
          id: profile.id,
          user: profile.username
        });


        uService
          .getUserWithProvider(profile.username, 'twitter')
          .then(function (result) {
            const user = new User();

            user.username = profile.username;
            user.role = UserRole.Maintainer;
            user.active = 1;

            if (result !== null) {
              user.password = profile.password;
              user.role = result.role;
              user.id = result.id;
              user.role = result.role;
            }
            user.provider = profile.provider;
            user.username = profile.username;
            user.identifier = profile.id;
            user.thumbnailurl = profile.photos.length > 0 ? profile.photos[0].value : null;
            user.bannerurl = profile._json.profile_banner_url;
            return uService.insertOrUpdate(user);
          })
          .then((user) => {
            logger.debug('Authentication has been validated, proceeding.', {
              username: user.username
            });
            done(null, user);
          })
          .catch(function (error) {
            logger.error('Error while persisting user.', {
              user: profile.username,
              failure: error.toString()
            });

            return done(null, false, {
              message: 'genericServerError'
            });
          });
      }));

  }
}


*/
