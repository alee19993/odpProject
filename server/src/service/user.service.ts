import {inject, injectable} from 'inversify';
import {User, UserDal} from '../models/user.model';
import TYPES from '../constant/types';
import {IUser} from '../interfaces/user.interface';

@injectable()
export class UserService implements IUser {


    constructor(@inject(TYPES.UserModel) public userModel: UserDal) {
    }

    create(user: User): Promise<User> {
        return this.userModel.create(user);
    }

    getAll(): Promise<User[]> {
        return this.userModel.getAll();
    }

    getUserById(userId: String): Promise<User> {
        return this.userModel.getUserById(userId);
    }

    update(user: User): Promise<User> {
        return this.userModel.update(user);
    }

    delete(userId: String): Promise<User> {
        return this.userModel.delete(userId);
    }

    getUsersByFilter(key: String, value: Number | String): Promise<User[]> {
        return  this.userModel.getUsersByFilter(key, value);
    }

}
