import {IService} from "../interfaces/service.interface";
import {inject, injectable} from "inversify";
import {Service, ServiceDal} from "../models/service.model";
import TYPES from "../constant/types";

@injectable()
export class ServiceService implements IService {
    constructor(@inject(TYPES.ServiceModel) private serviceModel: ServiceDal){}

    create(service: Service): Promise<Service> {
        return this.serviceModel.create(service);
    }

    getAll(): Promise<Service[]> {
        return this.serviceModel.getAll();
    }

    getServiceById(serviceId: String): Promise<Service> {
        return this.serviceModel.getServiceById(serviceId);
    }

    update(service: Service): Promise<Service> {
        return this.serviceModel.update(service);
    }

    delete(serviceId: String): Promise<Service> {
        return this.serviceModel.delete(serviceId);
    }

    getServicesByFilter(key: String, value: Number | String): Promise<Service[]> {
        return this.serviceModel.getServicesByFilter(key, value);
    }

}