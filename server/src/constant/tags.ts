const TAGS = {
    UserController: 'UserController',
    AuthController: 'AuthController',
    ServiceController: 'ServiceController'
};

export default TAGS;
