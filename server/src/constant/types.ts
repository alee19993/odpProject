import {LocalAuthStrategy} from '../auth/strategies/local-strategy';

const TYPES = {
    MongoDBClient: Symbol.for('MongoDBClient'),
    ServiceService: Symbol.for('ServiceService'),
    ServiceModel: Symbol.for('ServiceDal'),
    UserService: Symbol.for('UserService'),
    UserModel: Symbol.for('UserDal'),
    LocalAuthStrategy: Symbol.for('LocalAuthStrategy'),
    MongoConnection: Symbol.for('MongoConnection'),
};

export default TYPES;
