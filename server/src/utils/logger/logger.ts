import { Logger, transports } from 'winston';
export const logger = new Logger({
    transports: [
        new transports.Console({
            colorize: true,
            level: 'debug',
            timestamp: true
        }),
    ],
});
export const accessLogger = new Logger({
    transports: [
        new transports.File ({ filename: 'logs/accessLogger.log',
            level: 'debug',
            timestamp: true }),
    ],
});
accessLogger.stream = {
    write: function (message: any, encoding: any) {
        accessLogger.debug(message);
    }
};

