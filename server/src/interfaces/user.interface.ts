import {User} from '../models/user.model';

export interface IUser {
    create(user:User): Promise<User>;
    getAll(): Promise<User[]>;
    getUserById(userId: String): Promise<User>;
    update(user:User): Promise<User>;
    delete(userId: String): Promise<User>;
    getUsersByFilter(key: String, value: Number | String): Promise<User[]>;
}