import {Service} from "../models/service.model";

export interface IService {
    create(service:Service): Promise<Service>;
    getAll(): Promise<Service[]>;
    getServiceById(serviceId: String): Promise<Service>;
    update(service:Service): Promise<Service>;
    delete(serviceId: String): Promise<Service>;
    getServicesByFilter(key: String, value: Number | String): Promise<Service[]>;
}