import * as passport from 'passport';

passport.serializeUser((user: any, done: any) => {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

export {passport};
