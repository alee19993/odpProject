import * as mongoose from 'mongoose';
import {logger} from '../utils/logger/logger';
import {injectable} from "inversify";
import config from '../config/index';

@injectable()
export class MongoConnection {
    constructor() {
        (<any>mongoose).Promise = global.Promise;
        let conn = mongoose.connect(config.mongo.uri+ config.mongo.dbName,  { useMongoClient: true })
            .then( () => {
                logger.info('MONGO - connected');
                return mongoose.connection;
            }).catch( (err) => {
                logger.error('MONGO - Error on connection ' + err);
            })
    }

}

